/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.flowershop.bo;

import info.hccis.flowershop.dao.FlowerOrderDAO;
import info.hccis.flowershop.entity.jpa.FlowerOrder;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Logan
 */
public class FlowerOrderBO {
    
    /**
     * Select all orders from database
     * 
     * @author LRM
     * @since 20201015
     * @return ArrayList<Order> orders
     */
    public ArrayList<FlowerOrder> selectAllOrders(){
        FlowerOrderDAO orderDAO = new FlowerOrderDAO();
        return orderDAO.selectAllOrders();
    }
    
    /**
     * Get customer names from database table
     * 
     * @author LRM
     * @since 20201015
     * @return 
     */
    public HashMap<Integer, String> getCustomerNames(){
        FlowerOrderDAO orderDAO = new FlowerOrderDAO();
        return orderDAO.getCustomerNames();
    }
    
    /**
     * Get flower names from database table
     * 
     * @author LRM
     * @since 20201015
     * @return 
     */
    public HashMap<Integer, String> getFlowers(){
        FlowerOrderDAO orderDAO = new FlowerOrderDAO();
        return orderDAO.getFlowers();
    }
    
    /**
     * Get the order status types from database
     * 
     * @author LRM
     * @since 20201016
     * @return HashMap of order status ids and values
     */
    public HashMap<Integer, String> getOrderStatuses(){
        FlowerOrderDAO orderDAO = new FlowerOrderDAO();
        return orderDAO.getOrderStatuses();
    }
    
    /**
     * Insert new order into database table
     * 
     * @author LRM
     * @since 20201016
     * @param order
     * @return 
     */
//    public boolean insert(FlowerOrder order){
//        FlowerOrderDAO orderDAO = new FlowerOrderDAO();        
//        return orderDAO.insert(order);
//    }
    
    public BigDecimal totalCost(FlowerOrder order){
        FlowerOrderDAO orderDAO = new FlowerOrderDAO();
        HashMap<Integer, Integer> costs = orderDAO.getItemCosts();
        double cost;

        double cost1 = order.getItem1() * costs.get(1);
        double cost2 = order.getItem2() * costs.get(2);
        double cost3 = order.getItem3() * costs.get(3);
        double cost4 = order.getItem4() * costs.get(4);
        double cost5 = order.getItem5() * costs.get(5);
        double cost6 = order.getItem6() * costs.get(6);
        
        cost = cost1 + cost2 + cost3 + cost4 + cost5 + cost6;
        
        BigDecimal bdCost = new BigDecimal(cost, MathContext.DECIMAL64);
        
        return bdCost;
    }
    
    public boolean delete(FlowerOrder order){
        FlowerOrderDAO orderDAO = new FlowerOrderDAO();
        return orderDAO.delete(order);
    }
    
    public boolean insert(FlowerOrder order){
        FlowerOrderDAO orderDAO = new FlowerOrderDAO();        
        return orderDAO.insert(order);
    }
}
