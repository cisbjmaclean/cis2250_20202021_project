/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.flowershop.repositories;

import info.hccis.flowershop.entity.jpa.FlowerOrder;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Logan
 */
public interface FlowerOrderRepository extends CrudRepository<FlowerOrder, Integer>
{  
    ArrayList<FlowerOrder> findAllByCustomerId(int customerId);
}
