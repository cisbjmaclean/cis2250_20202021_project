/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.flowershop.dao;

import info.hccis.flowershop.entity.jpa.FlowerOrder;
import info.hccis.flowershop.util.CisUtility;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class with methods related to Orders database class
 * 
 * @author LRM
 * @since 20201014
 */
public class FlowerOrderDAO {
    private String userName=null, password=null, connectionString=null;
    private Connection conn = null;
    
    public FlowerOrderDAO(){
        String propFileName = "application";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        System.out.println("bjtest datasource:  " + rb.getString("spring.datasource.url"));
        connectionString = rb.getString("spring.datasource.url");
        userName = rb.getString("spring.datasource.username");
        password = rb.getString("spring.datasource.password");

        try {
            conn = DriverManager.getConnection(
                    connectionString, userName, password);
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    } 
    
    /**
     * Method to allow user to insert data into database from form
     * 
     * @author LRM
     * @since 20201015
     * @param order
     * @return 
     */
    public boolean insert(FlowerOrder order){
        try{            
            String insertSQL = "INSERT INTO flowerorder(customerId, item1, item2, item3, item4, item5, item6, orderDate, orderStatus, totalCost, amountPaid) "
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?)";
            
            PreparedStatement stmt = conn.prepareStatement(insertSQL);
            stmt.setInt(1, order.getCustomerId());
            stmt.setInt(2, order.getItem1());
            stmt.setInt(3, order.getItem2());
            stmt.setInt(4, order.getItem3());
            stmt.setInt(5, order.getItem4());
            stmt.setInt(6, order.getItem5());
            stmt.setInt(7, order.getItem6());
            stmt.setString(8, order.getOrderDate());
            stmt.setInt(9, order.getOrderStatus());
            stmt.setBigDecimal(10, order.getTotalCost());
            stmt.setBigDecimal(11, order.getAmountPaid());
            
            stmt.executeUpdate();
            return true;
            
        }catch (SQLException ex) {
            System.out.println("Error selecting bookings. (" + ex.getMessage() + ")");
            return false;
        }
    }

    /**
     * Select all orders from the database
     * @since 2020-10-14
     * @author LRM
     */
    public ArrayList<FlowerOrder> selectAllOrders() {
        ArrayList<FlowerOrder> orders = new ArrayList();
        try {
            Statement stmt = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = stmt.executeQuery("SELECT * FROM flowerorder");

            //Show all the bookers
            while (rs.next()) {

                int id = rs.getInt("id");
                int custId = rs.getInt("customerId");
                String orderDate = rs.getString("orderDate");
                int item1 = rs.getInt("item1");
                int item2 = rs.getInt("item2");
                int item3 = rs.getInt("item3");
                int item4 = rs.getInt("item4");
                int item5 = rs.getInt("item5");
                int item6 = rs.getInt("item6");
                int orderStatus = rs.getInt("orderStatus");
                //BigDecimal totalCost = rs.getDouble("totalCost");
                BigDecimal totalCost = rs.getBigDecimal("totalCost");
                BigDecimal amountPaid = rs.getBigDecimal("amountPaid");

                FlowerOrder order = new FlowerOrder(id);
                order.setCustomerId(custId);
                order.setOrderDate(orderDate);
                order.setItem1(item1);
                order.setItem2(item2);
                order.setItem3(item3);
                order.setItem4(item4);
                order.setItem5(item5);
                order.setItem6(item6);
                order.setOrderStatus(orderStatus);
                order.setTotalCost(totalCost);
                order.setAmountPaid(amountPaid);

                orders.add(order);
                System.out.println("Order Id: " + rs.getInt("id"));
                //System.out.println("Booker for id: " + rs.getString("id") + " is " + rs.getString("name1"));
            }
        } catch (SQLException ex) {
            System.out.println("Error selecting bookings. (" + ex.getMessage() + ")");

        }
        return orders;
    }
    
    public HashMap<Integer, String> getCustomerNames(){
        HashMap<Integer, String> names = new HashMap<Integer, String>();
        
        try{
            Statement stmt = conn.createStatement();
            String query = "SELECT id, fullName FROM customer";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                int id = rs.getInt("id");
                String fullName = rs.getString("fullName");
                names.put(id, fullName);
            }
        }catch(SQLException ex){
            System.out.println("Error find names. (" + ex.getMessage() + ")");
        }
        return names;
    }
    
    public HashMap<Integer, String> getFlowers(){
        HashMap<Integer, String> flowers = new HashMap<Integer, String>();
        
        try{
            Statement stmt = conn.createStatement();
            String query = "SELECT id, description FROM itemtype WHERE id < 4";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                int id = rs.getInt("id");
                String description = rs.getString("description");
                flowers.put(id, description);
            }
            
        }catch(SQLException ex){
            System.out.println("Error find names. (" + ex.getMessage() + ")");
        }
        
        return flowers;
    }
    
    public HashMap<Integer, String> getOrderStatuses(){
        HashMap<Integer, String> statuses = new HashMap<Integer,String>();
        
        try{
            Statement stmt = conn.createStatement();
            String query = "SELECT id, description FROM orderstatustype";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                int id = rs.getInt("id");
                String description = rs.getString("description");
                statuses.put(id, description);
            }
        }catch(SQLException ex){
            System.out.println("Error find names. (" + ex.getMessage() + ")");
        }
        return statuses;
    }
    
    public HashMap<Integer, Integer> getItemCosts(){
        HashMap<Integer, Integer> costs = new HashMap<Integer,Integer>();
        try{
            Statement stmt = conn.createStatement();
            String query = "SELECT id, cost FROM itemtype";
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                int id = rs.getInt("id");
                int cost = rs.getInt("cost");
                costs.put(id, cost);
            }
        }catch(SQLException ex){
            System.out.println("Error find names. (" + ex.getMessage() + ")");
        }
        return costs;
    }
    
    public boolean delete(FlowerOrder order){

        return false;
    }
}
