package info.hccis.flowershop.controllers;

import info.hccis.flowershop.bo.CustomerTypeBO;
import info.hccis.flowershop.entity.jpa.CustomerType;
import info.hccis.flowershop.repositories.CustomerTypeRepository;
import info.hccis.flowershop.util.CisUtility;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BaseController {

    private final CustomerTypeRepository customerTypeRepository;

    public BaseController(CustomerTypeRepository ctr) {
        customerTypeRepository = ctr;
    }
    
    @RequestMapping("/")
    public String home(HttpSession session) {
        
        //BJM 20200602 Issue#1 Set the current date in the session
        String currentDate = CisUtility.getCurrentDate("yyyy-MM-dd");
        session.setAttribute("currentDate", currentDate); 
        
        ArrayList<CustomerType> customerTypes = (ArrayList<CustomerType>) customerTypeRepository.findAll();
        session.setAttribute("customerTypes", customerTypes);
        System.out.println("BJM, loaded "+customerTypes.size()+" customer types");
        
        HashMap<Integer, String> customerTypesMap = CustomerTypeBO.getCustomerTypesMap();
        customerTypesMap.clear();
        for(CustomerType current: customerTypes){
            customerTypesMap.put(current.getId(), current.getDescription());
        }
        
        return "index";
    }

    @RequestMapping("/about")
    public String about() {
        return "other/about";
    }
}
